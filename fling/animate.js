function main(){

  var Marble = function(){

    this.check = function(){
      var value;
      var callFn = $(this).attr('flyIn');
      $.ajax({
        type: "get",
        url: "http://stormy-chamber-1319.herokuapp.com/check?callback=?",
        dataType: "jsonp",
        async: false,
        success: function(jsonData) {
          value = jsonData["value"];
          if(value == "one"){
            callFn();
          }
        },
        failure: function() {
          $("body").text(jsonData);
        }
      });
    };
  };

  Marble.prototype.flyIn = function(){
    $("#marble").animate({bottom:"+=800px"},"slow");
    $("#marble").animate({top:"+=700px"},"slow",
     function(){
       $("#board").attr('src',"images/kahuna4.jpg");
       $("#knife").animate({bottom:"+=800px"},"slow");
       $("#knife").css({"webkit-animation-name": "spinnerRotate"});
     });
  };

  var marble = new Marble();
  setInterval(function(){marble.check()},2000);  

}

$(main);