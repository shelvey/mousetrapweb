function main(){

  var Mouse = function(){
    var intPosition = 0;
    this.nextMove = function(){
      this.check();
      if(intPosition == 0){
        this.moveRight();
        intPosition++;
      } 
      else if(intPosition == 1){
        this.moveLeft();
        intPosition++;
      }
      else if(intPosition == 2){
        this.moveLeft();
        intPosition++;
      }       
      else if(intPosition == 3){
        this.moveRight();
        intPosition = 0;
      }       
    };
  };

  Mouse.prototype.check = function(){
    var value;
    $.ajax({
      type: "get",
      url: "http://stormy-chamber-1319.herokuapp.com/check?callback=?",
      dataType: "jsonp",
      async: false,
      success: function(jsonData) {
        value = jsonData["value"];
        if(value == "two"){
          $("#cage").animate({top:"+=300px"});
        }       
      },
      failure: function() {
        $("body").text(jsonData);
      }
    });
  };

  Mouse.prototype.moveRight = function(){
    $("#mouse").animate({right:"-=600px"});
  };
    
  Mouse.prototype.moveLeft = function(){
    $("#mouse").animate({right:"+=600px"});
  };

  var mouse = new Mouse();
  setInterval(function(){mouse.nextMove()},5000);

}

$(main);
